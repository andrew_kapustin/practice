// Одним из элементов браузерного окружения является DOM 
// Document Object Model - объектная модель документа, которая представляет все содержимое страницы в виде объектов

// document. ...
 

// DOM - это представление HTML-документа в виде дерева тегов (каждый HTML-тег является объектом)

// Типы узлов:
// - document (входная точка в DOM)
// - элемент (тег)
// - текст (пробел / перенос строки *)
// - комментарий


// ЭЛЕМЕНТЫ ЭТО ТЕГИ - NODE это узлы(элементы, текст, пробел, перенос строки, комментарий) !!!!!!!!!!!!


// querySelectorAll вернет NodeList
// getElementsByClassName вернет HTMLCollection

// Все дочерние элементы, включая текстовые находятся в массиве childNodes
// firstChild, lastChild, parentNode 
// previousSibling, nextSibling  - дочерние элементы стоящие рядом


// ______________________________________________________________________________________________________________________________


// Browser Object Model – это Объектная модель браузера дополнительные объекты, предоставляемые браузером (окружением), чтобы работать со всем, кроме документа

// С помощью объектной модели браузера (BOM) Вы можете управлять поведением браузера из JavaScript

// Объект window является корневым объектом JavaScript. Все объекты JavaScript, а также переменные и функции определяемые пользователем хранятся в объекте window.

// Объект Location - содержит информацию о текущем URL
// Объект Naviagator - содержит информацию о браузере пользователя 
// Объект History - содержит URL которые были посещены на этой вкладке 
// Объект Screen - содержит информацию о экране пользователя 
// Объект Frames - содержит информавцию о всех frame 


// // Свойсвта:
// window.innerHeight – внутренняя высота окна браузера (в пикселях)
// window.innerWidth – внутренняя ширина окна браузера (в пикселях)


// document.body.clientHeight - (область просмотра)
// document.body.clientWidth - (область просмотра)



// Методы:

// window.open() - открывает новое окно
// window.close() - закрывает текущее окно
// window.moveTo() - передвигает текущее окно
// window.resizeTo() - изменяет размер текущего окна