// Массив
// let array = []; // 1 вариант 
// let array = new Array(); // 2 вариант 

// let fruits = ['Яблоко', 'Апельсин', 'Хурма', 'Виноград'];
// console.log(fruits);
// fruits[2] = 'Груша';


// МЕТОДЫ
// PUSH() - добавить элемент в конец
// fruits.push('Груша');

// POP() - удаляет последний элемент из массива и возвращает его
// let popElement = fruits.pop();
// console.log(fruits)
// console.log(popElement)

// SHIFT() - удаляет первый элемент массива и возращает его 
// let shiftElement = fruits.shift();
// console.log(shiftElement);
// console.log(fruits);

// UNSHIFT() - добавляет элемент в начало массива 
// fruits.unshift('Ананас');
// console.log(fruits);


// SPLICE - arr.splice( startItemIndex, countItems, *putElemetns*); - возращает массив удаленных элементов
// let arr = fruits.splice(2,1);
// console.log(fruits)
// console.log(arr)

// SLICE() - arr.slice(startItem, endItem) - возвращает новый массив, в который копирует элементы, начиная с индекса start и до end (не включая end). Массив не изменяется !!!!
// let arr = fruits.slice(1, 3);
// console.log(arr);
// console.log(fruits);

// CONCAT() - объеденяет нессколько массивов в один
// let arr1 = [2, 4, 5, 6, 0];
// let arr2 = ['qqq', 'www', 'rrr'];
// arr1 = arr1.concat(arr2);
// console.log(arr1)

// FOREACH() - выполняет указанную функцию один раз для каждого элемента в массиве
// arr.forEach(function callback(currentValue(Текущий обрабатываемый элемент в массиве.), index(Индекс текущего обрабатываемого элемента в массиве.), array(Массив, по которому осуществляется проход.)) {
//     //your iterator
// }[, thisArg]);
// let arr = [1, 2, 3, 4, 5, 6];
// arr.forEach( function(element) {
//     console.log(Math.pow(element, 2))
// });



// FOR .. OF ()
// for(let fruit of fruits) {
//     console.log(fruit);
// }



// indexOf(item, from) /  - начиная с индекса from, и возвращает индекс, на котором был найден искомый элемент, в противном случае -1.
// lastIndexOf(item, from) / - то же самое, но ищет справа налево.
//includes(item, from) - ищет item, начиная с индекса from, и возвращает true, если поиск успешен.
// console.log(fruits.indexOf('Апельсин'));
// console.log(fruits.lastIndexOf('Апельсин'));
// console.log(fruits.includes('Апельсин'));


// FIND( (item, index, arry) => ... ) 
// let users = [
//     {id: 1, name: "Вася"},
//     {id: 2, name: "Петя"},
//     {id: 3, name: "Маша"},
//     {id: 4, name: "Маша"}
// ];
// // console.log(users.find(item => item.id == 1));
// console.log(users.find(item => item.name == "Маша"));



// // FINDINDEX()
// console.log(users.findIndex(item => item.id == 1)); // вернется индекс элемента в массиве


// FILTER( (item, index, array) => () ); - возвращает массив из всех подходящих элементов
// console.log(users.filter(item => item.id > 1));



// MAP( (item, index, array) => ... ) - Он вызывает функцию для каждого элемента массива и возвращает массив результатов выполнения этой функции.
// console.log( arr.map( items => Math.pow(items, 2)) );



// SORT( function() {...} )
// let arr = [45, 12, 56, 4, 6, 29, 49, 36]; - меняет массив
// console.log(arr.sort());
// console.log(arr)


// REVERSE() - МЕНЯТЕ МАССИВ,  переворачивает его 


// SPLIT( delim ) -  разбивает строку на массив по заданному разделителю delim
// let names = 'Вася, Петя, Маша';
// let arr = names.split(', ');
// for (let name of arr) {
//   console.log( `Сообщение получат: ${name}.` ); // Сообщение получат: Вася (и другие имена)
// }




// JOIN( glue ) -  создаёт строку из элементов arr, вставляя glue между ними
// let arr = ['Вася', 'Петя', 'Маша'];
// let str = arr.join(';'); // объединить массив в строку через ;
// alert( str ); // Вася;Петя;Маша



// Если нам нужно перебрать массив – мы можем использовать forEach, for или for..of




// REDUCE() - используются для вычисления какого-нибудь единого значения на основе всего массива
// let arr = [1, 2, 3, 4, 5, 6];
// console.log( arr.reduce( (accumalateValue, currentValue) => accumalateValue + currentValue, 0 ) ); // 1 + 2, 3+3, 6+4, 10+5, 15+6;




// console.log(fruits); // ["Яблоко", "Апельсин", "Хурма", "Виноград", "Груша"]
// let arr = fruits; // передача по ссылке, две переменные ссылаются на один и тот же массив
// arr.pop(); //
// console.log(fruits); // ["Яблоко", "Апельсин", "Хурма", "Виноград"]
// console.log(arr); // ["Яблоко", "Апельсин", "Хурма", "Виноград"]




// 
// function countVowelLetters(sentence) {
//     let vowels = ['а','у','е','ё','ы','о','э','я','и','ю'];
//     let count = 0;
//     let letters = sentence.split('');
//     letters.forEach( item => {
//         vowels.map( vouelItem => vouelItem == item ? count++ : undefined);
//     });
//     return count;
// }
// console.log(countVowelLetters("Пришла зима, запахло…"));
// console.log(countVowelLetters("Ghbdtn, z r dfv bp Hjccbb")); 
// console.log(countVowelLetters("длинношеее"));
// console.log(countVowelLetters("Не будете ли Вы так любезны, Иван, передать мне блокнот и «Известия»"));






// ХЭШ
// let user = {
//     name: "andrew",
//     age: 21,
//     isStudent: true,
//     email: "andrew@gmail.com"
// }
// console.log(user);

// for(let key in user){
//     console.log(key);
//     console.log(user[key]);
// }






// SET
// let set = new Set(); - каждое значение может появляться только один раз
// let arr = [1, 2, 1, 2, 2, 1, 2, 5, 6];
// let set = new Set();
// let set1 = new Set(arr);
// set.add('444');


// console.log(set1.delete(7))

// set1.has('rrr') // true/false

// clear() - удаляет все значения 
// size() - выводит колличксво элементов в коллекции 
// console.log(set);





// MAP - ключ это любое значение 
// new Map() – создаёт коллекцию.
// let myMap = new Map();


// map.set(key, value) – записывает по ключу key значение value
// myMap.set(true, 'car');
// myMap.set(234, 80);


// map.get(key) – возвращает значение по ключу или undefined, если ключ key отсутствует.
// myMap.get(true);


// map.has(key) – возвращает true, если ключ key присутствует в коллекции, иначе false.
// myMap.set(true); // true 
// myMap.set(34); // false


// map.delete(key) – удаляет элемент по ключу key.
// myMap.set(true);  

// map.clear() – очищает коллекцию от всех элементов.
// myMap.clear();


// map.size – возвращает текущее количество элементов.
// myMap.size();




// SPREAD
// let belarusCities = ['Минск', 'Брест', 'Витебск', 'Гомель', 'Молгилев', 'Гродно'];
// let europeCities = ['Берлин', 'Париж', 'Лондон', 'Прага'];

// //spread разворачивает массив и делает строку из него, так же служит для объединения массивов
// let allCities = [...belarusCities, 'Калифорния', ...europeCities];
// //равносильно 
// let allCities = belarusCities.concat(europeCities);

// spread в объектах
// let belarusCitiesPopulation = {
//     Minsk: 5,
//     Gomel: 4,
//     Brest: 2
// }
// let europeCitiesPopulation = {
//     Minsk: 7,
//     Paris: 20,
//     Moscow: 15,
//     London: 10
// }

// let newObjBelarus = {...belarusCitiesPopulation}; // клонирование объекта 
// let newObjAllCities = {...belarusCitiesPopulation, ...europeCitiesPopulation}; // Minsk: 7 так как перезаписывается по последнему



// REST - собирает все параметры в новый массив / объект 
// function sum(a, b) {
//     return a + b;
// }
// let arr = [1, 2, 3, 4, 5];
// sum(arr); // 3

// function sum(a, b, ...rest) {
//     console.log(rest); // [3, 4, 5];
//     console.log(...rest); // 3 4 5
//     return a + b + rest.reduce( (acume, item) => acume + item,0);
// }
// let arr = [1, 2, 3, 4, 5];
// sum(...arr)



// Диструктуризация
// let newArr = [1, 2, 3, 4, 5, 6, 7, 8];
// // let a = newArr[0]; // 1
// // let b = newArr[1]; // 2
// let [a, b, ...others] = newArr;
// // console.log(a) // 1
// console.log(b) // 2
// console.log(others); // [3, 4, 5, 6, 7, 8]


const person = {
    name: "Andrew",
    age: 21,
    city: "Minsk",
    country: "Belarus"
}
const {name, age, ...address} = person;
console.log(name, age, address);