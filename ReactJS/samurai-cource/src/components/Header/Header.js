import React from 'react'

import classes from './Header.module.css'


const Header = () => {
    return(
        <header className={classes.header}>
            <img src="https://thedreadnoughts.com/wp-content/uploads/2018/06/instagram-logo-white-on-black.png"></img>
        </header>
    );
}

export default Header;