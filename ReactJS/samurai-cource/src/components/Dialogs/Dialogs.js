import React from "react";
import classes from './Dialogs.module.css'

import Dialog from "./Dialog/Dialog";
import Contacts from "./Contacts/Contacts";
import ContactsNav from "./ContactsNav/ContactsNav";

function Dialogs(props) {
    return(
        <div className={classes.wrapper}>
            <ContactsNav></ContactsNav>
            <Contacts></Contacts>
            <Dialog></Dialog>
        </div>
    );
}

export default Dialogs;
