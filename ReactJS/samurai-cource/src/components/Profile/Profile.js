import React from 'react'
import classes from './Profile.module.css'
import MyPosts from './MyPosts/MyPosts'

const Profile = () => {
    return(
        <div className={`${classes.content}`}>
            <div className={`${classes.ava} ${classes.bg_profile}`}>
                ava
            </div>
            <div className={`${classes.description} ${classes.bg_profile}`}>
                description
            </div>
            <div className={`${classes.info} ${classes.bg_profile}`}>

            </div>
            <MyPosts></MyPosts>
        </div>
    );
}

export default Profile;