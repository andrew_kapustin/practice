// SOLID

// 1) Single Responsibility Principle – каждый класс отвечает за свой функционал

// class News {
//     constructor(title, text) {
//         this.title = title;
//         this.text = text;
//         this.modified = false;
//     }
//     update(text) {
//         this.text = text;
//         this.modified = true;
//     }
// }

// class NewsPrinter {
//     constructor(news){
//         this.news = news;
//     }
//     html() {
//         return `
//             <div class="news">
//                 <h1>${this.news.title}</h1>
//                 <p>${this.news.text}</p>
//             </div>
//         `
//     }
//     json() {
//         return JSON.stringify({
//             title: this.news.title,
//             text: this.news.text,
//             modified: this.news.modified
//         });
//     }
// }
// // const printer = new NewsPrinter(new News('Covid', 'Statictics for 20.05'));
// const news = new News('Covid', 'Statictics for 20.05');
// const printer = new NewsPrinter(news);
// console.log(printer.html());
// console.log(printer.json());

// ___________________________________________________________________________________________________________________________________

// 2) Open Closed Principle - классы нельзя модифицировать, но можно расширять

// class Shape {
//     area(){
//         throw new Error ('Area method should be implemented');
//     }
// }
// class Square extends Shape {
//     constructor(size) {
//         super();
//         this.size = size;
//     }
//     area() {
//         return this.size ** 2;
//     }
// }
// class Circle extends Shape {
//     constructor(radius) {
//         super();
//         this.radius = radius;
//     }
//     area() {
//         return (this.radius ** 2) * Math.PI;
//     }
// }
// class Rect extends Shape {
//     constructor(width, height) {
//         super();
//         this.width = width;
//         this.height = height;
//     }
//     area() {
//         return this.width * this.height;
//     }
// }
// class AreaCalculator {
//     constructor(shapes = []) {
//         this.shapes = shapes;
//     }
//     sum() {
//         return this.shapes.reduce( (acc, shape) => {
//             acc += shape.area();
//             return acc;
//         }, 0);
//     }
// }
// const calc = new AreaCalculator([
//     new Square(5),
//     new Rect(10, 4),
// ]);
// console.log(calc.sum());


// ___________________________________________________________________________________________________________________________________

// 3) Liskov Substitution Principle - правильный выбор абстракций 

// class Person{
//     constructor() {
//     }
// }
// class Member extends Person{
//     access() {
//         console.log('У тебя есть доступ !');
//     }
// }
// class Guest extends Person{
//     isGuest = true;
//     access() {
//         throw new Error('У вас нет права доступа !')
//     }
// }
// class FreontEnd extends Member {
// }
// class BackEnd extends Member {
// }
// class EmploeeFromAnotherCompany extends Guest {
// }

// function openSecretDoor(person) {
//     person.access();
// }
// openSecretDoor(new FreontEnd() )
// openSecretDoor(new BackEnd() )
// openSecretDoor(new EmploeeFromAnotherCompany() )


// ___________________________________________________________________________________________________________________________________


// 4) Interface Segregation Principle - если потомки класа не используют методы родителтского класса

// class Animal {
//     constructor(name) {
//         this.name = name;
//     }
// }
// const walker = {
//     walk() {
//         console.log(`${this.name} умеет ходить !`);
//     }
// }
// const flyer = {
//     fly() {
//         console.log(`${this.name} умеет летать !`);
//     }
// }
// const swimmer = {
//     swim() {
//         console.log(`${this.name} умеет плавать !`);
//     }
// }
// class Dog extends Animal {};
// class Eagle extends Animal {}
// class Whale extends Animal {}

// Object.assign(Dog.prototype, walker, swimmer);
// Object.assign(Eagle.prototype, flyer, walker);
// Object.assign(Whale.prototype, swimmer);

// const dog = new Dog('собака');
// dog.walk();
// dog.swim();

// const eagle = new Eagle('орел');
// eagle.fly();
// eagle.walk();

// const whale = new Whale('кит');
// whale.swim();



// ___________________________________________________________________________________________________________________________________

// 5) Dependency Inversion Principle