// ООП – объектно-ориентированное программирование, шаблон решения программ, в основе которого лежат объекты и их взаимодействие
// В терминах ООП отделение внутреннего интерфейса от внешнего называется инкапсуляция.


// ИНКАПСУЛЯЦИЯ –  сокрытие состояния объекта от прямого доступа извне. По умолчанию все свойства объектов являются публичными, общедоступными, и мы к ним можем обратиться из любого места программы.


// class User {
//     _age = 1;
//     constructor(name) {
//         this._name = name;
//     }
//     setName(name) {
//         this._name = name;
//     }
//     getName() {
//         return this._name;
//     }
//     setAge(age) {
//         this._age = age;
//     }
//     getAge() {
//         return this._age;
//     }
// }
// let user = new User('Andrew');




// class User{
//     #age = 0;
//     constructor(name){
//         this.name = name;
//     }
    
//     displayInfo() {
//         console.log(`Имя ${this.name}; возраст ${this.#age}`);
//     }
//     getAge() {
//         return this.#age;
//     }
//     setAge(age){
//         this.#age = age;
//     }
// }
// var tom = new User("Том");
// // console.log(tom); // User {name: "Том", #age: 0}
// // console.log(tom.name); //'Tom'



// *************************************************************************************************************************************

// НАСЛЕДОВАНИЕ

// class Animal{
//     constructor(name) {
//         this.speed = 0;
//         this.name = name;
//     }
//     run(speed) {
//         this.speed = speed;
//         console.log(`${this.name} бежит со скоростью ${this.speed}.`);
//     }
//     stop() {
//         this.speed = 0;
//         console.log(`${this.name} стоит.`);
//     }
// }
// let animal = new Animal('Мой питомец');


// class Rabbit{
//     constructor(name) {
//         this.name = name;
//     }
//     hide() {
//         console.log(`${this.name} прячется !!!`);
//     }
// }
// let rabbit = new Rabbit('Мой кролик');


// class Rabbit extends Animal{
//     hide() {
//         console.log(`${this.name} прячется!`);
//     }
//     stop() {
//         super.stop();
//         this.hide();
//     }
// }
// let rabbit = new Rabbit("Белый кролик");
// rabbit.run(10);
// rabbit.stop();

// _____________________________________________________________________________________________________________________________________

// class Animal {
//     constructor(name) {
//         this.name = name;
//         this.speed = 0;
//     }
//     // 
// }

// class Rabbit extends Animal{
//     constructor(name, earLength){
//         super(name);
//         this.earLength = earLength;
//     }
//     // 
// }
// let rabbit = new Rabbit('Белый кролик', 10);
// console.log(rabbit.name);
// console.log(rabbit.earLength);


// *************************************************************************************************************************************
// ПРОЛИМОРФИЗМ

// class User{
//     constructor(name, age){
//         this.name = name;
//         this.age = age;
//     }
//     getInfo(){
//         return `Меня зовут ${this.name}, мне ${this.age}`;
//     }
//     getName(){
//         return this.name;
//     }
// }
// let user = new User('Андрей', 21);

// class Admin extends User{
//     constructor(name, age, possition){
//         super(name, age);
//         this.possition = possition;
//     }
//     getInfo(){
//         return `${this.age} админ ${this.name} явяется ${this.possition}`
//     }
// }
// let admin = new Admin('Ivan', 30, 'Master-Admin');

// console.log(user.getInfo());
// console.log(admin.getInfo());


// *************************************************************************************************************************************


// Ассоциация это такой тип при котором объекты будут ссылаться друг на друга. При этом они остаются полностью независимыми друг от друга.

// Агрегация - 
// class Salary {
// 	constructor(pay, bonus) {
// 		this.pay = pay;
// 		this.bonus = bonus;
// 	}

// 	annual_salary() {
// 		return (this.pay * 12) + this.bonus;
// 	}
// }
// class Employee {
// 	constructor(name, age, salary) {
// 		this.name = name;
// 		this.age = age;
// 		this.salary = salary;
// 	}

// 	total_salary() {
// 		if (this.salary) {
// 			return this.salary.annual_salary(); // !!!!!!!
// 		}
// 	}
// }
// let salary = new Salary(15000, 10000)
// let emp = new Employee('Max', 25, salary)
// console.log(emp.total_salary())


// ___________________________________________________________________________________________________________________________________


// Композиция 

// class Salary {
// 	constructor(pay, bonus) {
// 		this.pay = pay;
// 		this.bonus = bonus;
// 	}
// 	annual_salary() {
// 		return (this.pay * 12) + this.bonus;
// 	}
// }
// class Employee {
// 	constructor(name, age, pay, bonus) {
// 		this.name = name;
// 		this.age = age;
// 		this.salary = new Salary(pay, bonus);
// 	}
// 	total_salary() {
// 		return this.salary.annual_salary();
// 	}
// }
// let emp = Employee('Max', 25, 15000, 10000)
// console.log(emp.total_salary())



// ___________________________________________________________________________________________________________________________________


// Ассоциация  

// class Salary {
// 	constructor(pay, bonus) {
// 		this.pay = pay;
// 		this.bonus = bonus;
// 	}
// 	annual_salary() {
// 		return (this.pay * 12) + this.bonus;
// 	}
// }
// class Employee {
// 	constructor(name, age) {
// 		this.name = name;
// 		this.age = age;
// 		this.salary = null;
// 	}
// 	total_salary() {
// 		if (this.salary) {
// 			return this.salary.annual_salary();
// 		}
// 	}
// }
// let emp = Employee('Max', 25)
// emp.salary = new Salary(15000, 10000)
// console.log(emp.total_salary())




// *************************************************************************************************************************************


// Внутренний интерфейс – это свойства и методы, доступ к которым может быть осуществлён только из других методов объекта, их также называют «приватными» (есть и другие термины, встретим их далее).
// class User{
//     _age = 1;

//     constructor(name) {
//         this.name = name;
//     }
//     displayInfo() {
//         console.log("Имя: " + this.name + "; возраст: " + this._age);
//     }
//     getAge() {
//         return this._age
//     }
//     setAge(age){
//         if(typeof age === "number" && age >0 && age<110){
//             this._age = age;
//         } else {
//             console.log("Недопустимое значение");
//         }
//     }
// }
// var user = new User('Tom');
// user.setAge(21);



// Внешний интерфейс – это свойства и методы, доступные снаружи объекта, их называют «публичными».

// class User{
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }
//     displayInfo() {
//         console.log("Имя: " + this.name + "; возраст: " + this._age);
//     }
// }
// var user = new User('Tom', 30);
// user.age = 35;
// let coffeeMachine = new CoffeeMachine(100);




// *************************************************************************************************************************************

// class User{
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }

//     saySmth() {} // абстрактный класс
// }

// *************************************************************************************************************************************
