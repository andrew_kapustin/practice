const square = document.querySelector('.square'); // через атрибут
const triangle = document.querySelector('.triangle'); // через DOM свойсвто on<событие>
const round = document.querySelector('.round'); // через члушателя
const container = document.querySelector('.container');


triangle.onclick = function() {
    console.log('Привет, Треугольник')
}

//

// назначить более одного обработчика так нельзя черзе атрибут и через DOM.
// 
// square.onclick = function() { // тем самым событие через атрибут будет кдалено и заменится на новое назвначенное событие 
//     console.log('Дубль два')
// }
// Внутри обработчика события this ссылается на текущий элемент - выводит блок с его содержанием




// ____________________________________________________________

round.addEventListener('click', function(event) {
    // event.stopPropagation();
    console.log('Привет, круг')
})
// round.onclick = function() { 
//     console.log('Дубль два')
// }
// round.addEventListener('click', function() {
//     console.log('Привет, 345')
// })

// выведет все три log 

// !!!!!!!!!!!!!!

// element.addEventListener(event, handler[, options]);
// event - Имя события, например "click".

// handler - Ссылка на функцию-обработчик.

// options - Дополнительный объект со свойствами:
// once: если true, тогда обработчик будет автоматически удалён после выполнения.
// capture: фаза, на которой должен сработать обработчик, подробнее об этом будет рассказано в главе Всплытие и погружение. Так исторически сложилось, что options может быть false/true, это то же самое, что {capture: false/true}.
// passive: если true, то указывает, что обработчик никогда не вызовет preventDefault(), подробнее об этом будет рассказано в главе Действия браузера по умолчанию.

// ____________________________________________________________

// всплытие
// Когда на элементе происходит событие, обработчики сначала срабатывают на нём, потом на его родителе, затем выше и так далее, вверх по цепочке предков.

container.addEventListener('click', function(event) {
    console.log('всплытие Клика')
    // console.log(event.currentTarget);
    // console.log(event.target); // при нажатии на paralell выведет его 
    console.log(event.bubbles);

})
// при нажатии на дочерний элемент, вызовет его обработчик а после и обработчик container



// Прекращение всплытия
// То есть, event.stopPropagation() препятствует продвижению события дальше, но на текущем элементе все обработчики будут вызваны.

// Для того, чтобы полностью остановить обработку, существует метод event.stopImmediatePropagation(). Он не только предотвращает всплытие, но и останавливает обработку событий на текущем элементе.

// stopPropagation / stopImmediatePropagation останавливает все события такого типо, если это click 

// container.addEventListener('mouseup', function() { // отработает везде, так как обработчик не клик !!!!
//     console.log('всплытие мыши')
// })


// triangle.removeEventListener('click', function() {
//     console.log('Привет, Треугольник')
// })


const paralell = document.querySelector('.paralell');

paralell.addEventListener('click', function(event) {
    console.log('Это Параллелограмм !')
    // event.stopPropagation(); // не вызывает слушатели при всплытии 
    // event.stopImmediatePropagation(); // не вызывает остальные слушатели на этом блоке и выше

    // event.preventDefault(); // отменяте стандартное действие браузера

    // свойства объекта EVENT
    // console.log(event.type) // click
    // console.log(event.target) // <div class="paralell"></div> //  сгенерировал событие
    // console.log(event.currentTarget) // <div class="paralell"></div> //  вызвал обработчик события
    // console.log(event.eventPhase); // (1 - этапе погружения (перехвата), 2 - на цели, 3 - на этапе всплытия)
    // console.log(event.timeStamp); // число (дата), когда произошло событие
    // console.log(event.bubbles); // возвращает логическое значение, указывающее может ли данное событие всплывать
    // console.log(event.defaultPrevented); // можно ли вызвать метод preventDefault() для данного события
    // console.log(event.view); // возвращает ссылку на объект window, в котором произошло событие


    // console.log(event.which); // (1 - левая кнопка, 2 - средняя кнопка, 3 - правая кнопка)
    // console.log(event.clientX, event.clientY); // координаты мыши при нажатии относительно левого верхнего угла клиентской области
    // console.log(event.screenX, event.screenY); // координаты мыши при нажатии относительно левого верхнего угла экрана
    // console.log(event.detail); // сколько раз была нажата кнопка мыши

})
paralell.addEventListener('click', function(event) {
    console.log('Второй слушатель')
})
paralell.addEventListener('click', function() {
    console.log('Третий слушатель')
})

paralell.dispatchEvent(new Event('click')); // создает событие клик на эелементе paralell