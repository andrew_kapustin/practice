// ООП – объектно-ориентированное программирование, шаблон решения программ, в основе которого лежат объекты и их взаимодействие
// В терминах ООП отделение внутреннего интерфейса от внешнего называется инкапсуляция.


// ИНКАПСУЛЯЦИЯ –  механизм объединения свойств и методов работающего с данными в один объект и сокрытие реализации от пользователя 

// - сокрытие состояния объекта от прямого доступа извне. По умолчанию все свойства объектов являются публичными, общедоступными, и мы к ним можем обратиться из любого места программы.

// приватные поля

// class User{
//     #age = 0;
//     constructor(name){
//         this.name = name;
//     }
    
//     displayInfo() {
//         console.log(`Имя ${this.name}; возраст ${this.#age}`);
//     }
//     getAge() {
//         return this.#age;
//     }
//     setAge(age){
//         this.#age = age;
//     }
// }
// var tom = new User("Том");
// // console.log(tom); // User {name: "Том", #age: 0}
// // console.log(tom.name); //'Tom'


// **********************************************************************


// const createCounter = function() {
//     let count = 0;
//     return ({
//       click: function() {
//           return count += 1;
//       },
//       getCount: () => count.toLocaleString()
//     });
// }

// const counter = createCounter();
// counter.click();
// counter.click();
// counter.click();
// console.log(
//   counter.getCount()
// );


// ________________________________________________________________________________________________________________________



// НАСЛЕДОВАНИЕ

// class Animal {
//     constructor(name) {
//         this.name = name;
//         this.speed = 0;
//     }
// }

// class Rabbit extends Animal{
//     constructor(name, earLength){
//         super(name);
//         this.earLength = earLength;
//     }
// }
// let rabbit = new Rabbit('Белый кролик', 10);
// console.log(rabbit.name);
// console.log(rabbit.earLength);



// ________________________________________________________________________________________________________________________



// ПРОЛИМОРФИЗМ

// class User{
//     constructor(name, age){
//         this.name = name;
//         this.age = age;
//     }
//     getInfo(){
//         return `Меня зовут ${this.name}, мне ${this.age}`;
//     }
//     getName(){
//         return this.name;
//     }
// }
// let user = new User('Андрей', 21);

// class Admin extends User{
//     constructor(name, age, possition){
//         super(name, age);
//         this.possition = possition;
//     }
//     getInfo(){
//         return `${this.age} админ ${this.name} явяется ${this.possition}`
//     }
// }
// let admin = new Admin('Ivan', 30, 'Master-Admin');

// console.log(user.getInfo());
// console.log(admin.getInfo());


// ________________________________________________________________________________________________________________________



// Ассоциация это такой тип при котором объекты будут ссылаться друг на друга. При этом они остаются полностью независимыми друг от друга.

// class Salary {
// 	constructor(pay, bonus) {
// 		this.pay = pay;
// 		this.bonus = bonus;
// 	}
// 	annual_salary() {
// 		return (this.pay * 12) + this.bonus;
// 	}
// }
// class Employee {
// 	constructor(name, age) {
// 		this.name = name;
// 		this.age = age;
// 		this.salary = null;
// 	}
// 	total_salary() {
// 		if (this.salary) {
// 			return this.salary.annual_salary();
// 		}
// 	}
// }
// let emp = Employee('Max', 25)
// emp.salary = new Salary(15000, 10000)
// console.log(emp.total_salary())






// Агрегация это тип отношений когда один объект является частью другого.

// class Salary {
//     constructor(pay, bonus) {
//         this.pay = pay;
//         this.bonus = bonus;
//     }

//     annual_salary() {
//         return (this.pay * 12) + this.bonus;
//     }
// }

// class Employee {
    // constructor(name, age, salary) { !!!!
//         this.name = name;
//         this.age = age;
//         this.salary = salary;
//     }

//     total_salary() {
//         if (this.salary) {
//             return this.salary.annual_salary(); // !!!!!!!
//         }
//     }
// }
// let salary = new Salary(15000, 10000)
// let emp = new Employee('Max', 25, salary)
// console.log(emp.total_salary());






// Композиция это тип отношений при котором один объект может принадлежать только другому объекту и никому другому.

// class Salary {
// 	constructor(pay, bonus) {
// 		this.pay = pay;
// 		this.bonus = bonus;
// 	}
// 	annual_salary() {
// 		return (this.pay * 12) + this.bonus;
// 	}
// }
// class Employee {
// 	constructor(name, age, pay, bonus) {
// 		this.name = name;
// 		this.age = age;
// 		this.salary = new Salary(pay, bonus);
// 	}
// 	total_salary() {
// 		return this.salary.annual_salary();
// 	}
// }
// let emp = Employee('Max', 25, 15000, 10000)
// console.log(emp.total_salary())


// *************************************************************************************************************************************


// Внутренний интерфейс – это свойства и методы, доступ к которым может быть осуществлён только из других методов объекта, их также называют «приватными».
// class User{
//     _age = 1;

//     constructor(name) {
//         this.name = name;
//     }
//     displayInfo() {
//         console.log("Имя: " + this.name + "; возраст: " + this._age);
//     }
//     getAge() {
//         return this._age
//     }
//     setAge(age){
//         if(typeof age === "number" && age >0 && age<110){
//             this._age = age;
//         } else {
//             console.log("Недопустимое значение");
//         }
//     }
// }
// var user = new User('Tom');
// user.setAge(21);



// Внешний интерфейс – это свойства и методы, доступные снаружи объекта, их называют «публичными».

// class User{
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }
//     displayInfo() {
//         console.log("Имя: " + this.name + "; возраст: " + this._age);
//     }
// }
// var user = new User('Tom', 30);
// user.age = 35;
// let coffeeMachine = new CoffeeMachine(100);




// *************************************************************************************************************************************

// class User{
//     constructor(name, age) {
//         this.name = name;
//         this.age = age;
//     }

//     saySmth() {} // абстрактный класс
// }

// *************************************************************************************************************************************
