// let / const / var

// 1 - область видимости let {...}
var apples = 5;
if (true) {
  var apples = 10;
  alert(apples); // 10 (внутри блока)
}
alert(apples); // 10 (снаружи блока то же самое)



let apples = 5; // (*)
if (true) {
  let apples = 10;
  alert(apples); // 10 (внутри блока)
}
alert(apples); // 5 (снаружи блока значение не изменилось)


// 2 - Переменная let видна только после объявления
alert(a); // undefined
var a = 5;

alert(a); // is not defined
let a = 5;


// 3 При использовании в цикле, для каждой итерации создаётся своя переменная
for(var i = 0; i < 10; i++) {
    console.log(i)
}



// Функции
function hi() { // Function Declaration = hoisting
    console.log('hi')
}
hi();

let sayHi = function() { //Function Expression
    console.log('Привет')
}
sayHi();

let sum = (a, b) => a + b; // стрелочная функция 
let ageFunc = age => age >= 18 ? alert('Добрый день') : alert('Привет');

( function() { // самовызов функции
	alert('Проверка функции')
})();
