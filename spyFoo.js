const foo = function (arg1, arg2, ... argN){}

function counter() {
    function toCount() {
        return toCount.count += 1;
    }
    toCount.count = 0;

    return toCount;
}
const toCount = counter();

function spy(func) {
    function innerSpy (...arg) {
        console.log(...arg);
        toCount();
        innerSpy.count = toCount.count;
        return func();
    }
    innerSpy.count = 0;
    return innerSpy;
}
const spyFoo = spy(foo);
spyFoo(1,2,3)// console.log(1,2,3)
console.log(spyFoo.count)// 1
spyFoo(1,2,3)
spyFoo(1,2,3)
console.log(spyFoo.count)// 3

