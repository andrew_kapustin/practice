// Типы данных 

// ЧИСЛО
// число, Infinity, -Infinity и NaN.

let number1 = 12;
let number2 = Infinity;
let number3 = -Infinity;
let number4 = NaN; 

// СТРОКА
// '' "" ``
let string1 = 'фраза в одинарных ковычках';
let string2 = "фраза в двойных ковычках";
let string1 = `фраза в косых ковычках`;

// БУЛЕВА ПЕРЕМЕННАЯ
// true / false
let isCorrect1 = 4 > 1;
let isCorrect2 = 4 > 10;

// NULL
let age = null;

// UNDEFINED - переменная объявлена но ей не присвоенно значение
let myAge;
console.log(myAge);

// OBJECT
let user = {
    name: "Andrew",
    age: 21,
    isStudent: true
}

// SYMBOL - уникальный идентификатор
let id = Symbol('myId'); // символ id с описанием myId

let newUser = {
    name: 'Ivan'
}
let usersId = Symbol('id');
user[usersId] = 1;




// ____________________________________

// Операторы языка
//  унарный - пременяется к одному операнду
let x = 1;
x = -x

// бинарный 

// тетрарный
let age = 20;
let status = (age >= 18) ? 'adult' : 'minor'

// switch
let side = 'down'
switch(side){
    case 'up': 
        console.log('верхняя сторона');
        break;
    case 'down': 
        console.log('нижняя сторона');
        break;
    case 'left': 
        console.log('левая сторона');
        break;
    case 'right': 
        console.log('правая сторона');
        break;
    default: 
        console.log('неизвестная сторона');
        break;
}

// while
let whileNumb = 1;
while ( whileNumb < 30 ) {
    console.log( `переменная whileNumb = ${whileNumb}` );
    whileNumb++;
}


// do while
let doWhileNumb = 1;
do{
    console.log( `переменная whileNumb = ${whileNumb}` );
    doWhileNumb++;
} while( doWhileNumb < 30 );

// for
for(let i = 0; i < 10; i++) {
    console.log(i)
}

