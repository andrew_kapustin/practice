// Контекст выполнения — это место выполнения кода. Код всегда выполняется внутри некоего контекста. контекстом называют значение переменной this внутри функции.
// function hello() {
//     console.log('hello', this); 
//   }
//   hello(); //this = window


//   const person = {
//     name: "andrew",
//     age:21,
//     sayHi: hello
//   }
//   person.sayHi(); // this = person


// this указывает на объект в контексте которого он был вызван



function hello() {
        console.log('hello', this); 
    }
    hello(); //this = window
    
    
    let person = {
        name: "Andrew",
        age:21,
        city: "Minsk",
        sayHi: hello,
        // sayHiWindow: hello.bind(window),
        logInfo: function() {
            console.log(`Имя ${this.name}`);
            console.log(`возвраст ${this.age}`);
            console.log(`город ${this.city}`);
        }
    }
    let ivan = {
      name: "Ivan",
      age: 30
    }
    // person.logInfo.bind(ivan)();
    // person.logInfo.call(ivan, 'Gomel')
    person.logInfo.apply(ivan, ['ty'])



    // В пределах функции значение this зависит от того, каким образом вызвана функция.
    function f1(){
        return this;
    }
    f1() === window;

// НО
function f2(){
    "use strict";
    return this;
  }
  f2() === undefined; // true



//   В стрелочных функциях, this привязан к окружению, в котором была создана функция. В глобальной области видимости this будет указывать на глобальный объект.


// Для доступа к информации внутри объекта метод может использовать ключевое слово this.
// Значение this – это объект «перед точкой», который использовался для вызова метода.

// let user = {
//     name: "Джон",
//     age: 30,
//     sayHi() {
//       alert(this.name);
//     }
//   };
  
//   user.sayHi(); // Джон