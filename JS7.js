// Лексическео окружение - скрытый объект функции состоящий из:
// - Environment Record (объект со всеми локальными переменными)
// - Ссылка на внешнее лексическое окружение, там где функция была объявлена!!!! (scope)
// Лексическео окружение создается ВО ВРЕМЯ ВЫЗОВА ФУНКЦИИ и емеет все переменные что созданы внутри и ссылку на внешнее лексическое окружение

//  1) globalLE = {scope: null}

function show() {
    //  5) showLE = {scope: globalLE}
    let number = 10;
    //  6) showLE = {scope: globalLE, number: 10}


    function showNumber() {
        //  8) showNumberLE = {scope: showLE}

        console.log(number);
    }

    showNumber();
    //  7) showLE = {scope: globalLE, number: 10, showNumber: f}


    console.log(text);
}


const text = 'Something ...';
// 2) globalLE = {scope: null, show: f, text: 'Something ...'}

const number = 5;
// 3) globalLE = {scope: null, show: f, text: 'Something ...', number: 5}


show();
// 4) globalLE = {scope: null, show: f, text: 'Something ...', number: 5, show: f}
// 10
// Something...





function sayHi(name) {
     // LexicalEnvironment = {scope: globalLE, name: 'Вася', phrase: undefined } - на стадии инициализации, интерпретатор создаёт пустой объект LexicalEnvironment и заполняет его
    var phrase = "Привет, " + name;
    // LexicalEnvironment = {scope: globalLE, name: 'Вася', phrase: 'Привет, Вася'} - о время выполнения происходит присвоение локальной переменной phrase, то есть, другими словами, присвоение свойству LexicalEnvironment.phrase нового значения
    alert( phrase );
}
  
sayHi('Вася');


// новое лексическое окружение функции создаётся каждый раз, когда функция выполняется. И, если функция вызывается несколько раз, то для каждого вызова будет своё лексическое окружение, со своими, специфичными для этого вызова, локальными переменными и параметрами.
let name = "John";
function sayHi() {
  console.log("Hi, " + name);
}
sayHi(); // John
name = "Pete"; 
sayHi(); // Pete


   


// Замыкание - когда из одной функции возвращаем новую функцию, тогда новая функция замкнута нf область видисомти родительской функции
// Замыкание - это комбинация функции и лексического окружения, в котором эта функция была определена
// Замыкание дает функции возможность продлить доступ к лексической области видимости, которая была определена на этапе разработки.



function counter() {
    let count = 0;
    
    return function() {
        return ++count;
    }
}

let firstCount = counter();
console.log( firstCount() );// 1
console.log( firstCount() ); //2
console.log( firstCount() ); //3
console.log( firstCount() ); //4

let anotherCounter = counter()
console.log( anotherCounter() ); //1
console.log( anotherCounter() ); //2
console.log( anotherCounter() ); //3
console.log( anotherCounter() ); //4



