let date = new Date();
// console.log(date); - текущая дата 

// ________________________________________________________________________

// методы 

// 1) getDate(): возвращает день (число) месяца
// console.log(date.getDate());

// 2) getDay(): возвращает день недели (отсчет начинается с 0 - воскресенье, и последний день - 6 - суббота)
// console.log(date.getDay());

// 3) getMonth(): возвращает номер месяца (отсчет начинается с нуля, то есть месяц с номер 0 - январь)
// console.log(date.getMonth());


// 4) getFullYear(): возвращает год
// console.log(date.getFullYear());


// 5) toDateString(): возвращает полную дату в виде строки
// console.log(date.toDateString()); // Thu Apr 22 2021


// 6) getHours(): возвращает час (от 0 до 23)
// console.log(date.getHours());


// 7) getMinutes(): возвращает минуты (от 0 до 59)
// console.log(date.getMinutes());


// 8) getSeconds(): возвращает секунды (от 0 до 59)
// console.log(date.getSeconds());


// 9) getMilliseconds(): возвращает миллисекунды (от 0 до 999)
// console.log(date.getMilliseconds());


// 10) toTimeString(): возвращает полное время в виде строки
// console.log(date.toTimeString()); // 18:58:35 GMT+0300 (Москва, стандартное время)




// SET

// setDate(): установка дня в дате

// setMonth(): уставовка месяца (отсчет начинается с нуля, то есть месяц с номер 0 - январь)

// setFullYear(): устанавливает год

// setHours(): установка часа

// setMinutes(): установка минут

// setSeconds(): установка секунд

// setMilliseconds(): установка миллисекунд



// console.log(date.toDateString()); // Thu Apr 22 2021
// date.setMonth(1);
// console.log(date.toDateString()); // Mon Feb 22 2021


