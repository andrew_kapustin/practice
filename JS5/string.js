// let a = 'Andrew '
// let b = ' '; // lenght 1

// let str = 'Строчка с несколькими словами'

// Свойства:
// console.log(a.length); // 8 - пробелы считаются за элемент

//____________________________________________________________________________________

// методы

// 1) charAt() - Возвращает символ строки с указанным индексом (позицией)
// console.log(a.charAt(0)); // A 

// 2) charCodeAt() - Возвращает числовое значение Unicode символа, индекс которого был передан методу в качестве аргумента.
// console.log(a.charCodeAt(0));

// 3) concat() - Возвращает строку, содержащую результат объединения двух и более предоставленных строк.
// let firstWord = 'I',
//     secondWord = 'Love',
//     therdWord = 'JS'
// console.log(firstWord.concat(secondWord, therdWord));

// 4) fromCharCode() - Возвращает строку, созданную с помощью указанной последовательности значений символов Unicode
// console.log(String.fromCharCode(65, 66, 67)); // ABC

// 5) indexOf() - Возвращает позицию первого символа первого вхождения указанной подстроки в строке или -1, если подстрока не найдена.
// let title = 'Синий кит'
// console.log(title.indexOf(' ')); // 5
// console.log(title.indexOf('ий')); // 3

// 6) lastIndexOf() - С ПРАВА НАЛЕВО

// 7) localeCompare() - ?????

// 8) match() - ????

// 9) replace() - заменяет в строке иервый переданный элемент на второй
// let str1 = 'Прекрасный день'
// let str2 = 'вечер' 
// console.log(str1.replace('день', str2));

// 10) search() - поиск по строке - возращает номер символа с которого начинается
// console.log(str.search('несколькими'));

// 11) slice(start [, end]) - удаляет всю строку до указаного символа 
// console.log(str.slice(10));
// let str1 = 'long string'
// console.log(str1.slice(-4)); // ring
// console.log(str1.slice(-4, -1)); // rin


// 12) split() - возвращает массив со строками, делится по переданному аргументу (' ')
// console.log(str.split(' ')); // ["Строчка", "с", "несколькими", "словами"]

// 13) substr(start [, length]) - Позволяет извлечь подстроку из строки. Первый аргумент указывает индекс с которого нужно начать извлечение (не включая). Второй аргумент указывает количество символов, которое нужно извлечь. (включая)
// console.log(str.substr(2, 8)); // 'рочка с '

// 14) substring() - так же как и substr()

// 15) toLocaleLowerCase() - Преобразует символы строки в нижний регистр с учетом текущего языкового стандарта. 
// let str1 = 'CAPSLOCK'
// console.log(str1.toLocaleLowerCase());

// 16) toLocaleUpperCase() - Преобразует символы строки в верхний регистр с учетом текущего языкового стандарта.

// 17) toLowerCase() - Конвертирует все символы строки в нижний регистр и возвращает измененную строку.

// 18) toUpperCase() - Конвертирует все символы строки в верхний регистр и возвращает измененную строку.

// 19) toString() - Возвращает строковое представление объекта. 
// let str1 = 'string line'
// console.log(str1.toString());

// 20) trim() -  Удаляет пробелы в начале и конце строки и возвращает измененную строку.
// let str1 = '      space    '
// console.log(str1);
// console.log(str1.trim());

// 21) valueOf - возвращает примитивное значение объекта String в виде строкового типа

// 22) includes() / startsWith() / endsWith()
// console.log("Widget with id".includes("Widget")); // true
// console.log( "Widget".startsWith("Wid")); // true
// console.log("Widget".endsWith("get")); // true 

// 23) repeat(n)
// let str = 'hi!'
// console.log(str.repeat(5));


