// console.log(document.querySelector('.element')); // выводит первый элемент

// console.log(document.querySelectorAll('.element')); // выводит NodeList, NodeList статичен, при добавлении через JS элементов он их не покажет



// ___________________________________________________________________________________________________
// let container = document.querySelector('.container');
// console.log( container.childNodes ); // NodeList // из 13 элементов [4 - div, 4 переноса div, p, перенос p, text, comment, перенос в конце]
// console.log( container.parentNode ); // родительский узел
// console.log( container.nextSibling ); // следующий узел
// console.log( container.previousSibling ); // предыдущий узел
// console.log( container.firstChild ); // первый дочерний узел
// console.log( container.lastChild ); // последний дочерний узел

// style, meta, title, script - тоже является узлом



// console.log( container.children ); // HTMLCollection // из 5 элементов (5 тегов)
// console.log( container.parentElement ); // родительский элемент
// console.log( container.nextElementSibling ); // следующий элемент
// console.log( container.previousElementSibling ); // предыдущий элемент
// console.log( container.firstElementChild ); // первый дочерний элемент
// console.log( container.lastElementChild ); // последний дочерний элемент



// querySelectorAll вернет NodeList
// getElementsByClassName вернет HTMLCollection



// ___________________________________________________________________________________________________



// Browser Object Model – это Объектная модель браузера предоставляющая доступ к окну браузера и позволяет манипулировать им и его элементами.

// С помощью объектной модели браузера (BOM) Вы можете управлять поведением браузера из JavaScript

// Объект window является корневым объектом JavaScript. Все объекты JavaScript, а также переменные и функции определяемые пользователем хранятся в объекте window.


// Объект Location - содержит информацию о текущем URL
// Объект Naviagator - содержит информацию о браузере пользователя 
// Объект History - содержит URL которые были посещены на этой вкладке 
// Объект Screen - содержит информацию о экране пользователя 
// Объект Frames - содержит информавцию о всех frame 

// window.localStorage
// window.sessionStorage

// // Свойсвта:
// window.innerHeight – внутренняя высота окна браузера (в пикселях) viewport
// window.innerWidth – внутренняя ширина окна браузера (в пикселях) viewport


// document.body.clientHeight - (область просмотра)
// document.body.clientWidth - (область просмотра)





// Методы:

// window.open() - открывает новое окно
// window.close() - закрывает текущее окно
// window.moveTo() - передвигает текущее окно
// window.resizeTo() - изменяет размер текущего окна